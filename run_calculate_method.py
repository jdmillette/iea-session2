#!/usr/bin/env python3
import sys

def run_script():
    print('Running as a scipt')
    if len(sys.argv) != 4:
        sys.exit(-1)
    try:
        x = float(sys.argv[1])
        y = float(sys.argv[2])
        operator = sys.argv[3]
    except ValueError:
        print(f'Please make sure the first two arguments are numbers.')
    else:
        print('Results:', calculate(x,y,operator))



if __name__ == '__main__': # This will allow you to use this script in python. 
    run_script()
else:
    print('Imported')