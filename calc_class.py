#!/usr/bin/env python3
import math, sys

#Need to take in 3 params. Num1, Num2, Operator
# Need to create a total and allow users to use that total as number 1

class Calculator:
    def __init__(self): #could call clear directly
        self.total = 0
        self.history = []

    def add(self,num1, num2 = None):
        if not num2:
            num2 = self.total
        self.total = num1 + num2
        self.history.append(['add', [num1, num2]])
        return self.total

    def subtract(self,num1, num2 = None):
        if not num2:
            num2 = self.total
        self.total = num1 - num2
        self.history.append(['subtract', [num1, num2]])
        return self.total

    def multiply(self,num1, num2 = None):
        if not num2:
            num2 = self.total
        self.total = num1 * num2
        self.history.append(['multiply', [num1, num2]])
        return self.total

    def divide(self,num1, num2 = None):
        if not num2:
            num2 = self.total
        try:
            self.total = num1 / num2
            self.history.append(['divide', [num1, num2]])
        except ZeroDivisionError:
            print('You can not divide by zero! ..... defaulting total to 0')
            self.total = 0
        return self.total

    def eval(self, num1, num2, operator): #eval is technically the a default function within Python as well... should change name
        if operator == '+':
            self.add(num1, num2)
        elif operator == '-':
            self.subtract(num1,num2)
        elif operator == '*':
            self.multiply(num1,num2)
        elif operator == '/': 
            self.divide(num1,num2)
        return self.total
            

    def display(self):
        return self.total
    
    def clear(self):
        self.total = 0
        self.history = []
        return self.total # Don't need it to return anything. 

    def show_history(self):
        #Should probably loop through this 
        print(self.history)
            

