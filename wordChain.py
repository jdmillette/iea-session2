#!/usr/bin/env python3


#Steps:
# Need to use a file that has a long list of words
# Ask the user the starting word 
# Ask the user what word they want to end on
# loop through the word file and change one character at a time
    # Make each word their own list.
    # Compare elements in the two words to assure only 1 letter changed


def check_word(wrd):

    word_file = '/home/jmillette/code/iea-session2/words'
    with open (word_file) as f1:
        # This could be its own function. 
        # Function would be to check if word is in word file.
        # Can then move start_word and end_word into their own function
        # while True:
    
        real_words = f1.read()
        if wrd not in real_words:
            print(f'{wrd} is not a real word.')
            results = True
        #     #print('Please choose a new word.')
        #     # word = input(f'{word} not a real word. Please choose a new word.')
        #     # if new_word not in f1:
        #     #     new_word = (f'{new_word} not a real word. Enter a new word by only changing one character. ')
        #     # if stop_word not in f1 or len(word) != len(stop_word)::
        #     #    #print('Please choose a new end word. ')
        #     #     stop_word = = input(f'{stop_word} not a real word. Please choose a new stop word.')
        #     # # if len(word) != len(stop_word):
        #     # #     print('Please make your start word and end word the same length')
        #     # #     stop_word = = input('What word would you like to get too? ')
        #     # break
        else:
            results = False
        return results

def word():
    results = True
    while results == True:
        wrd = input('What word would you like to begin with? ').lower()
        results = check_word(wrd)
    return wrd

#Maybe combine this with the word function
def next_word():
    new_word = input('Enter a new word by only changing one character. ').lower()
    check_word(new_word)
    return new_word

def end_word():
    stop_word = input('What word would you like to get too? ').lower()
    check_word(stop_word)
    return stop_word


def compare_words(new_word, word):
    # Make each word a list. Try listcomp
    # Do a for in to check the conents of each list
    # Append differnt letters to a new list
    # Check to see if that new list length is greater than 1

    while True:
        word_difference = [ char for char in new_word 
                                if char not in word]
        if len(word_difference) > 1:
            print('Follow directions. Only change one character!')
            print(word_difference)
            next_word()
        break
    wrd = new_word
    word_difference = []
    return wrd


def main():
    wrd = word()
    stop_word = end_word()
    print(f'Starting word is {wrd} and ending word is {stop_word}.')
    while True:
        new_word = next_word()
        if new_word == stop_word:
            print('Congrats! You did it.')
            return False
        wrd = compare_words(new_word, wrd) #wrd does not get overwritten. Maybe have compare words only have 1
        print(new_word)
  
        


main()



