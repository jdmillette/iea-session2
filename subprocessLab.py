#!/usr/bin/env python3
import subprocess
import sys

#path = input('Path to see the space used: ')
path = sys.argv[1]
try:
    if len(sys.argv) == 2:
        s = subprocess.getoutput(f'du -s {path}')
        print(s)
except Exception  as e:
    print(f'{e}. Pleas try again')
