def calculate (x, y, operator):
    # could use eval on a string variabl. But it doesn't validate what a user passed. And it could be anythinng
    if operator == '+':
        total = x + y
    elif operator == '-':
        total = x -y
    elif operator == '*':
        total = x * y
    elif operator == '/':
        total = x / y
    elif operator == '//':
        total = x // y
    elif operator == '%':
        total = x % y
    elif operator == '**':
        total = x ** y
    else:
        total == x + y
    return total

print(calculate(2, 4, '*'))

#Steps:
# Break down number into individual number and add each one 
def sumdigits(num):
    """ If num = 1235 then it will 
    compute the sum of 1,2,3, and 5 (11)"""
    number_string = str(num)
    total = 0
    for digit in number_string:
        number = int(digit)
        total += number
    return total

print(sumdigits(1235))

#Steps
# Take input and then add comma for every third element
def add_commas(number):
    """ Add commas for every third number in arg.
        So 12345 will return 12,345 """
    number = str(number)
    # length_of_number = len(number)
    # index = length_of_number -1
    while len(number) > 3:
        if len(number) < 6:
            new_number = number[:2] + ',' + number[2:]
            return new_number
        elif len(number) >= 6 and len(number) < 9:
            new_number = number[:3] + ',' + number[3:]
            return new_number
        elif len(number) >= 9 and len(number) < 12:
            new_number = number[:4] + ',' + number[4:]
            return new_number
        elif len(number) >= 9 and len(number) < 12:
            new_number = number[:5] + ',' + number[5:]
            return new_number
    else:
        return number

print(add_commas(123456))

# Steps:
# check if number is % 2 == 0 then do the stated rule and reset n to that number
# if number  % 2 != 0 then do the other rule
# Stop when nnumber = 1
def collatz(number):
    """for integer n > 1
        if n is even, then n = n // 2
        if n is odd, then n = n * 3 + 1 """
    n = number
    while n != 1:
        if n % 2 == 0:
            n = n // 2
        else:
            n = n * 3 + 1
    return n

print(collatz(1007))



