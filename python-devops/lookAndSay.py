"""To generate a member of the sequence from the previous member, read off the digits of the previous member, 
counting the number of digits in groups of the same digit. For example:
    1 is read off as "one 1" or 11.
    11 is read off as "two 1s" or 21.
    21 is read off as "one 2, then one 1" or 1211.
    1211 is read off as "one 1, one 2, then two 1s" or 111221."""

#Steps:
# Loop over the input and count the number of occurnece
# Then add that number of occurence to the front of the input number.
# Continue until the user says stop

def start():
    number = input('Please enter your starting digit (1 -9): ')
    #sequence = 0
    return number

def to_continue():
    sequences = int(input('How many results do you want to see? '))
    #start_or_stop = input('Do you want to see the next sequence? y or n: ').lower()
    #sequence += 1
    return sequences

def logic(number):
    result = ''
    counter = 1
    length = len(number)
    for digit in range(length):
        if digit == length -1 or number[digit+1] != number[digit]:
            result += str(counter) + number[digit]
            counter = 1
        else:
            counter += 1
    number = result
    return number
                    
        #to_continue()
        #sequence += 1

def next_sequence(number,sequences):
    for i in range(sequences):
        print(number)
        number = logic(number)    

number = start()
sequences = to_continue()
next_sequence(number,sequences)



