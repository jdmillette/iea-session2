"""The player designated to go first says the number "1", and each player thenceforth counts one number in turn. However, any number divisible by three is replaced by the word fizz 
and any number divisible by five by the word buzz. Numbers divisible by 15 become fizz buzz. A player who hesitates or makes a mistake is eliminated from the game.

For example, a typical round of fizz buzz would start as follows:

1, 2, Fizz, 4, Buzz, Fizz, 7, 8, Fizz, Buzz, 11, Fizz, 13, 14, Fizz Buzz, 16, 17, Fizz, 19, Buzz, Fizz, 22, 23, Fizz, Buzz, 26, Fizz, 28, 29, Fizz Buzz, 31, 32, Fizz, 34, Buzz, Fizz,

try rewriting it with restrictions (one at a time):		
Implement using no loops
		Implement using no variables
		Implement using no "if" statements
"""
# Steps:
# Count off numbers start at 1 to 100
# if number % 3 == 0 then replace that number with Fizz
# if number % 5 == 0 replace number with buzz
x = int(input('How high would you like to go?'))

def countoff(x):
    output = []
    for num in range(1,x):
        output.append(num)
        if num % 3 == 0 and num % 15 != 0:
            output[num -1] = 'fizz'
        elif num % 5 == 0 and num % 15 != 0:
            output[num - 1] = 'buzz'
        elif num % 15 == 0:    # could add this to the top of the if statement and get rid of and "num % 15 != 0" on the other statements
            output[num - 1] = 'fizz buzz'
    return output

def make_list_element_strings(answers):
    #answers = ['char' for char in answers]
    string = []
    for num in answers:
        x = str(num)
        string.append(x)
    return string


def play(countoff):
    count = 0
    while True:
        count +=1
        guess = input(f'Should {count} be; itself. fizz, buzz, FIZZ BUZZ: ')
        if guess == string_answers[count-1]:
            print('Correct!')
        else:
            print('Incorrect. Game over! The right answer was ', string_answers[count - 1])
            break
        if count >= x:
            print('Good job! You completed it!')
            break

answers = countoff(x)
string_answers = make_list_element_strings(answers)
play(string_answers)

    