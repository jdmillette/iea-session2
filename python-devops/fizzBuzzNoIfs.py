"""The player designated to go first says the number "1", and each player thenceforth counts one number in turn. However, any number divisible by three is replaced by the word fizz 
and any number divisible by five by the word buzz. Numbers divisible by 15 become fizz buzz. A player who hesitates or makes a mistake is eliminated from the game.

For example, a typical round of fizz buzz would start as follows:

1, 2, Fizz, 4, Buzz, Fizz, 7, 8, Fizz, Buzz, 11, Fizz, 13, 14, Fizz Buzz, 16, 17, Fizz, 19, Buzz, Fizz, 22, 23, Fizz, Buzz, 26, Fizz, 28, 29, Fizz Buzz, 31, 32, Fizz, 34, Buzz, Fizz,

try rewriting it with restrictions (one at a time):		
        Implement using no loops
		Implement using no variables
		Implement using no "if" statements
"""
# Steps:
# Count off numbers start at 1 to 100
# if number % 3 == 0 then replace that number with Fizz
# if number % 5 == 0 replace number with buzz
x = int(input('How high would you like to go? '))

def countoff(x):
    output = [ number for number in range(1, x+1)]
    fizz(output)
    buzz(output)
    fizz_buzz(output)
    return output

def fizz (output):
    fizz = [ number for number in output[2::3]]
    index = 0
    for number in fizz:
        index = number -1
        output[index] = 'fizz'
    return output

def buzz(output):
    buzz = [number for number in output[4::5]]
    index = 0
    previous = 0
    for number in buzz:
        x = 0
        while number =='fizz':
            x = previous+5 
            output[x] = 'buzz'
            break
        while number != 'fizz':
            index = (number -1) + x
            output[index] = 'buzz'
            previous = index
            break
    return output

def fizz_buzz(output):
    fizz_buzz = [number for number in output[14::15]]
    start = 14
    index = 0
    previous = 0
    for number in fizz_buzz:
        while number =='fizz' or number == 'buzz':
            x = previous+14 
            while start != 14:
                output[x] = 'fizz buzz'
                start +=1
                break
            else: 
               #index == 14:
               output[x] = 'fizz buzz' 
               index += x
               break

        while number != 'fizz' and number != 'buzz':
            index = (number -1) + x
            output[index] = 'fizz buzz'
            previous = index
            break
        #index = number -1
        #output[index] = 'fizz buzz'
    return output

def make_list_element_strings(answers):
    #answers = ['char' for char in answers]
    string = []
    for num in answers:
        x = str(num)
        string.append(x)
    return string


def play(countoff):
    game = 'on'
    count = 1
    while game == 'on':
        guess = input(f'Should {count} be; itself. fizz, buzz, FIZZ BUZZ: ')
        while guess == string_answers[count-1]:
            print('Correct!')
            break
        else:
            print('Incorrect. Game over! The right answer was ', string_answers[count - 1])
            game = 'over'
        win(count,game)
        count +=1
         
def win(count,game):
    x = len(string_answers)
    while x == count:
        print('You win!')
        game = 'over'
        break
    return game

answers = countoff(x)
string_answers = make_list_element_strings(answers)
play(string_answers)

    