#!/usr/bin/env python3

# Steps:
# While loop untilk they type quit
# Ask user for input
# Loop through input string and ask user what they want to do
# Ask user to either add, remove, or reverse a list
# Display both lists

list_1 = []
list_2 = []

while True:
    list_function = input(f'Do you want to add to list1, list2, remove a string or index from list1 or list2, or reverse either list: ')
    if ('add' in list_function or 'remove' in list_function) and 'index' not in list_function:
        string = input('Please enter a string or type quit to exit: ')
        if string == 'quit':
            break

    if 'index' in list_function:
        index = int(input('What index number would you like to remove: '))
    if 'add' in list_function:
        if 'list1' in list_function or 'list 1' in list_function:
            list_1.append(string)
            print(list_1)
        else:
            list_2.append(string)
            print(list_2)
    elif 'reverse' in list_function:
        if 'list1' in list_function or 'list 1' in list_function:
            list_1 = list_1[::-1]
            print(list_1)
        else:
            list_2 = list_2[::-1]
            print(list_2)
    elif 'index' in list_function:
        if len(list_1) >= index and ('list1' in list_function or 'list 1' in list_function):
            index = index -1
            del list_1[index]
            print(list_1)
        elif len(list_2) >= index and ('list2' in list_function or 'list 2' in list_function):
            index = index -1
            del list_2[index]
            print(list_2)
        else:
            print(f'{index} is out of range... Skipping')
    else:
        if 'list1' in list_function or 'list 1' in list_function:
            while string in list_1:
                list_1.remove(string)
            print(list_1)
        else:
            while string in list_2:
                list_2.remove(string)
            print(list_2)
print(f'This is list 1: {list_1}')
print(f'This is list 2; {list_2}')

if list_1 in list_2:
    print('The two lists are the same!')
else:
    print('The two lists are not the same')
