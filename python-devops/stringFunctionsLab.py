#!/usr/bin/env python3
user_input = input('Enter a string: ')
stride = int(input('Enter a stride: '))
user_input_list = list(user_input)
start = 0
is_upper = True
for index in user_input[::stride]:
    if is_upper:
        #print(user_input_list[start:start + stride].upper(), end= '')
        upper = user_input[start:start + stride].upper()
        user_input_list[start:start + stride] = upper
        start += stride
        is_upper = False
    else:
        #print(user_input_list[start:start + stride].lower(), end='')
        lower = user_input[start:start + stride].lower()
        user_input_list[start:start + stride] = lower
        start += stride
        is_upper = True
print("".join(user_input_list))
