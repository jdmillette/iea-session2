PRICES = {
    'apple': 0.99,
    'banana': 2.99,
    'orange': 1.15,
    'grape': 1.99,
    'mango': 2.50
}

cart = {}
while True:
    print('Items:')
    for key, value in PRICES.items():
        print(key.capitalize(), '${}'.format(value))
    item = input('Add item to the cart, or enter to checkout:').lower()
    if not item:
        break
    if item not in PRICES:
        print('Sorry, we do not have that item!')
        continue
    cart_item = cart.setdefault(
        item, {'quantity': 0, 'price': PRICES[item]})
    cart_item['quantity'] += 1
    cart_item['price'] = cart_item['quantity'] * PRICES[item]
    print('Your Cart:')
    print('Qty', 'Item', 'Price')
    for k in cart:
        print(cart[k]['quantity'], k, cart[k]['price'])

# Apply promotions
apple_discount = -(PRICES['apple'] * 3 - 2.00)
discount_line = {
    'quantity': cart['apple']['quantity'] // 3,
    'price': cart['apple']['quantity'] // 3 * apple_discount
}
if discount_line['quantity']:
    cart['APPLES 3 FOR 2.00'] = discount_line
mango_discount = -PRICES['mango']
discount_line = {
    'quantity': cart['mango']['quantity'] // 2,
    'price': cart['mango']['quantity'] // 2 * mango_discount
}
if discount_line['quantity']:
    cart['MANGO BOGO'] = discount_line
orange_discount = -PRICES['orange']
discount_line = {
    'quantity': cart['banana']['quantity'] // 3,
    'price': cart['banana']['quantity'] // 3 * orange_discount
}
if discount_line['quantity']:
    cart['ORANGE FREE WITH 3 BANANA'] = discount_line

# Checkout
print('Your Cart:')
print('Qty', 'Item', 'Price')
total = 0
for k in cart:
    total += cart[k]['price']
    print(cart[k]['quantity'], k, cart[k]['price'])
print('Total:', total)
