#!/usr/bin/env python3

# Ask user for a list
# Look inside that list to see if char already exists within it
    # If so, remove that item from a list
string = input('Enter a list of items: ').split(' ')
updated_list = []
for word in string:
    if word not in updated_list:
        updated_list.append(word)
print('List without duplicates ', updated_list)
