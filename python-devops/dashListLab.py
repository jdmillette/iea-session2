#Steps:
# Loop through user input until they enter 'quit'
# Add those words from input to a list
#   If word begins with -, then we don't add it to the list
#   if only -, then we need to reverse the order
# Print thee list after each operation
word_list = []
while True:
    list_string = []
    string = input('Enter a word or word(s)')
    list_string = string.split(' ')
    print('This is the list of the string,', list_string, 'and its length', len(list_string))
    if string == 'quit':
        break
    if string == '-':
        word_list.reverse()
    elif string.startswith('-'):
        # list_string = string.split('-').split('')
        for word in list_string:
            word = word.split(' ')
            print(f'this is the {word}')
            while word in word_list:
                word_list.remove(word)
        # print(list_string)
        # if len(list_string) >= 2:
        #     for word in list_string:
        #         if word.startswith('-'):
        #             continue
        #         print(word)
        #         word_list.append(word)
    else:
        for word in list_string:
            word_list.append(word)
    print(word_list)
        
