def calculate(a, b, operator):
    if operator == '+':
        result = a + b
    elif operator == '-':
        result = a - b
    elif operator == '*':
        result = a * b
    elif operator == '/':
        try:
            result = a / b
        except ZeroDivisionError:
            print('You cannot divide by zero!')
            result = 0
    elif operator == 'log':
        try:
            result = math.log(a) / math.log(b)
        except (ValueError, TypeError, ZeroDivisionError):
            print('Please double-check your values!')
            result = 0
        else:
            print('No errors!')
        finally:
            print('Finally!')
    return result
