import sys

import calculate
#from calculate import calculate


def run_script():
    print('Running as a script')
    for idx, arg in enumerate(sys.argv):
        print(f'arg {idx} is {arg}')

    if len(sys.argv) != 4:
        print('Usage: calculate_argv OPERAND OPERAND OPERATOR')
        sys.exit(-1)
    try:
        x = float(sys.argv[1])
        y = float(sys.argv[2])
        operator = sys.argv[3]
    except ValueError:
        print('Invalid parameter! Please supply numeric values')
    except IndexError:
        print('Usage: calculate_argv OPERAND OPERAND OPERATOR')
    else:
        print('Result:', calculate.calculate(x, y, operator))


if __name__ == '__main__':
    run_script()
else:
    print('Imported')

