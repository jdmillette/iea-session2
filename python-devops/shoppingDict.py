""" Steps:
Create a dict for the items and their prices
Ask use what they want to purchase 
Then ask teh quantity
After the input, add that item if there to a cart
The cart should list out the items,  quanity and the total price
    Probably want to use the dict special functions on key, value and item
BONUS: Handle the following promotions:	
    Apples - 3 for 2.00
	Mango - Buy 1 Get 1 free
	Orange - Free with the purchase of 3 bunches of bananas
Probably best to handle the bonus at checkout. 
    Check to see if they meet the criteria for the sale and provide the discount
"""
inventory = {'Apple': 0.99, 'Banana': 2.99, 'Orange': 1.15, 'Grape': 1.99, 'Mango': 2.50}
cart = {}
# can turn this into a function shopping
while True:
    print(inventory)
    prompt = input('Please choose an item from above you would like to purchase or type x to quit: ')
    if prompt == 'x':
        break
    quantity = int(input(f'How many {prompt} would you like to buy: '))
    key = (quantity, prompt)

    if prompt in inventory:
        item_price = inventory.get(prompt)
    cart[key] = round(item_price * quantity , 2)
    
total = 0
for item in cart:
    price = cart.get(item)
    total += price

print(cart, 'Total:',total)