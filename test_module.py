#!/usr/bin/env python3

def greeting():
    greeting = ('How would you like to be greeted? ')
    return greeting

def name():
    name = input('What would you like to be called? ')
    return name

def name_and_greeting(greeting, name):
    print(f"I'll greet you by: {greeting} {name}")