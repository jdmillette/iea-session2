#!/usr/bin/env python3
import math

def calculate (x , y, operator):
    if operator == '+':
        total = x + y
    elif operator == '-':
        total = x -y
    elif operator == '*':
        total = x * y
    elif operator == '/': # could also throw the try execpt here as well
        try:
            total = x / y
        except ZeroDivisionError:
            print('You can not divide by zero! ..... defaulting total to 0')
            total = 0
    elif operator == '//':
        total = x // y
    elif operator == '%':
        total = x % y
    elif operator == '**':
        total = x ** y
    elif operator == 'log':
        try:
            #total = math.log(x) / math.log(y)
            log_x = math.log(x)
            log_y = math.log(y)

        #except Exception as e:
           # print(f'Error thrown: {e} ..... defaulting total to 0')
            #total = 0
        except (ValueError, TypeError, ZeroDivisionError):
            print('You are trying to log with an invalid number(0, negative number, ). Setting total to 0.')
            total = 0
        else:
            total = log_x / log_y
        # OR we can do recursion
        #else:
        # total = calculate(log_x,log_y, '/')
    #print(total)
    return total
