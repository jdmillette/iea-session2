#!/usr/bin/env python
import re


#Steps:
    # Have two inputs
        # Asking for a file
        # Asking for search criteria
    
user_file = input('What file would you like to open and search? ')
search = input('What do you want to search? ')

#Should allow user to cycle through 
def grep (user_file, search):
    linenum = 0
    try:
        for line in open(user_file):
            linenum += 1
            if re.search(r'{}'.format(search), line):
                result = re.search(r'{}'.format(search), line)
                result = result.group(0)
                # print(result)
                print('{}: {}'.format(linenum, 
                                re.sub(r'{}'.format(search), f'**{result}**', line)), end='')
    except FileNotFoundError:
         print(f'{user_file} does not exist')

grep(user_file,search)

# linenum = 0
# for line in open(user_file):
#     linenum += 1
#     if re.search(r'{}'.format(search), line):
#         result = re.search(r'{}'.format(search), line)
#         result = result.group(0)
#         # print(result)
#         print('{}: {}'.format(linenum, 
#                         re.sub(r'{}'.format(search), f'**{result}**', line)), end='')

#for line in open(user_file):
#             linenum += 1
#             if re.search(search, line):
#                 print(f'{linenum}: {line}', end='')