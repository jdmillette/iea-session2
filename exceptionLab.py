import math

def calculate (x, y, operator):
    # could use eval on a string variabl. But it doesn't validate what a user passed. And it could be anythinng
    if operator == '+':
        total = x + y
    elif operator == '-':
        total = x -y
    elif operator == '*':
        total = x * y
    elif operator == '/': # could also throw the try execpt here as well
        try:
            total = x / y
        except ZeroDivisionError:
            print('You can not divide by zero! ..... defaulting total to 0')
            total = 0 # Default total to 0
    elif operator == '//':
        total = x // y
    elif operator == '%':
        total = x % y
    elif operator == '**':
        total = x ** y
    elif operator == 'log':
        try:
            total = math.log(x) / math.log(y)

        except Exception as e:
            print(f'Error thrown: {e} ..... defaulting total to 0')
            total = 0

        # else:
        #     return total
    return total
    

# x = int(input('Enter the first number: '))
# y = int(input('Enter the second number: '))
# operator = input('What would opperand would you like to use: ')

print('Total: ', calculate(4,0,'/'))
