import random
# Two players
# Need to import/create a deck of cards
# The deck gets dealt evenly between the two players
#   look at the deckofcards docs
# loop through each user playing a card
# perfom an if statement to determine who won. Use pop to determine first card
# if a tie, players place 3 cards face down and flip the forth
# The player with the highest cards collects all cards played 
# Game continues until a player has no cards left

print("""Game rules:
Highest card wins the round and the winner takes the losers card and adds it to the bottom of their deck.
If the flip is a tie, theen it's a WAR. In this case, both users flip three cards.
Whoever's card is the highest is the winner and takes all the cards.
11s are Jacks, 12 are Queens, 13 are Kings, and 14 are Aces.""")

# deck = [2, 3, 4, 5, 6, 7, 8, 9, 10, 'J', 'Q', 'K', 'A'] * 4
#print(deck)
# card_value = {2 : 2, 3 : 3, 4 : 4, 5 : 5, 6 : 6, 7 : 7, 8 : 8, 9 : 9, 10 : 10, 'J': 11, 'Q': 12, 'K': 13, 'A':14}

#deck = [2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14] * 4

def shuffle_deck():
    deck = [2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14] * 4
    random.shuffle(deck)
    return deck

def pass_out_cards(deck):
    player1_deck = deck[:26]
    player2_deck = deck[26:]
    return player1_deck, player2_deck

def war (player1_deck,player2_deck):
    while True:
        index = 4
        print('Its a tie, WAR time. Flip 3 cards facedown and flip the fourth card ')
        if player1_deck[index] > player2_deck[index]:
            print(f'Player 1 wins war. {player1_deck[index]} beats {player2_deck[index]}')
            for card in player2_deck[:index]:
                player1_deck.append(card)
            #player1_deck.extends(player2_deck[:index])
            break
        elif player1_deck[index] < player2_deck[index]:
            print(f'Player 2 wins war. {player2_deck[index]} beats {player1_deck[index]}')
            for card in player1_deck[:index]:
                player2_deck.append(card)
            #player2_deck.extends(player1_deck[:index])
            break
        elif len(player1_deck) < 4:
            print('Player 2 wins! Player 1 ran out of cards.')
            break
        elif len(player2_deck) < 4:
            print('Player 1 wins! Player 2 ran out of cards.')
            break
            
        index += 4

def game_play(player1_deck,player2_deck):
    while True:
        play = input('Type y to keep playing and n to quit: ')
        if play == 'y':
            keep_playing = True
        else:
            keep_playing = False
        if keep_playing:
            if len(player1_deck) > 0 and len(player2_deck) > 0:
                player1_play = player1_deck.pop(0)
                player2_play = player2_deck.pop(0)

                print(f'Player 1s card: {player1_play}')
                print(f'Player 2s card: {player2_play}')

                if player1_play == player2_play:
                    war(player1_deck, player2_deck)
                    continue
                elif player1_play > player2_play:
                    print('Player 1 card is greater')
                    player1_deck.append(player2_play)
                else:
                    print('Player 2 card is greater')
                    player2_deck.append(player1_play)
            else:
                if len(player1_deck) == 0:
                    print('Player 1 ran out of cards, Player 2 wins!')
                    break
                else:
                    print('Player 2 ran out of cards, Player 1 wins!')
                    break
        else:
            x = len(player1_deck)
            y = len(player2_deck)
            if x == y:
                print('Its a tie!')
                break
            elif x > y:
                print('Player 1 has more cards, so they win!')
                break
            else:
                print('Player 2 has more cards, so they win!')
                break

shuffled_deck = shuffle_deck()
player1_deck, player2_deck = pass_out_cards(shuffled_deck) 
game_play(player1_deck, player2_deck)


print('Player 1 deck:', player1_deck)
print('Player 2 deck:', player2_deck)


