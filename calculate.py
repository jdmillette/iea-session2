#!/usr/bin/env python3
import math, sys

def calculate (x , y, operator):
    # try:
    #     x = float(sys.argv[1])
    #     y = float(sys.argv[2])
    #     operator = sys.argv[3]
    # except ValueError as e:
    #     print(f'Please make sure the first two arguments are numbers: {e}')

    # if len(sys.argv) != 4:
    #     # Will also be an index error
    #     raise Exception ('Invalid arguments passed. Please pass 2 numbers and then an operator') 


    # could use eval on a string variabl. But it doesn't validate what a user passed. And it could be anythinng
    if operator == '+':
        total = x + y
    elif operator == '-':
        total = x -y
    elif operator == '*':
        total = x * y
    elif operator == '/': # could also throw the try execpt here as well
        try:
            total = x / y
        except ZeroDivisionError:
            print('You can not divide by zero! ..... defaulting total to 0')
            total = 0
    elif operator == '//':
        total = x // y
    elif operator == '%':
        total = x % y
    elif operator == '**':
        total = x ** y
    elif operator == 'log':
        try:
            #total = math.log(x) / math.log(y)
            log_x = math.log(x)
            log_y = math.log(y)

        #except Exception as e:
           # print(f'Error thrown: {e} ..... defaulting total to 0')
            #total = 0
        except (ValueError, TypeError, ZeroDivisionError):
            print('You are trying to log with an invalid number(0, negative number, ). Setting total to 0.')
            total = 0
        else:
            total = log_x / log_y
        # OR we can do recursion
        #else:
        # total = calculate(log_x,log_y, '/')
    #print(total)
    return total

def run_script():
    print('Running as a scipt')
    if len(sys.argv) != 4:
        sys.exit(-1)
    try:
        x = float(sys.argv[1])
        y = float(sys.argv[2])
        operator = sys.argv[3]
    except ValueError:
        print(f'Please make sure the first two arguments are numbers.')
    else:
        print('Results:', calculate(x,y,operator))



if __name__ == '__main__': # This will allow you to use this script in python. 
    run_script()
else:
    print('Imported')

#print(calculate(int(sys.argv[1]), int(sys.argv[2]), sys.argv[3]))
#print('Total: ', calculate(4,1,'log'))
#calculate(4,2,'log')